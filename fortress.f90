program fortress
   implicit none

   integer, parameter  :: lim = 10
   integer, parameter  :: times = 10
   integer, dimension (times) :: means
   real :: u 

   integer, parameter :: seed = 86453
   integer :: a = 1
   integer :: b  =1

   call srand(seed)
   do a = 1,lim
     do b = 1,times
       means(a) = means(a) + floor(rand() * 6.0)
     end do
     write (*,*) 'Result:', means(a)
   end do

end program fortress
